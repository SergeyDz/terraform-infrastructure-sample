variable "instance_type" {
  description = "Azure instance type"
  default = "Standard_D4s_v3"
}

variable "location" {
  description = "Azure region"
  default = "West Europe"
}

variable "cloudinit_file" {
    description = "Ps file for cloud init"
    default = "init.ps1"
}

variable "subscription_id" {
    description = "subscription_id"
    default = "1"
}

variable "client_id" {
    description = "client_id"
    default = "2"
}

variable "client_secret" {
    description = "client_secret"
    default = "3"
}

variable "tenant_id" {
    description = "tenant_id"
    default = "4"
}
